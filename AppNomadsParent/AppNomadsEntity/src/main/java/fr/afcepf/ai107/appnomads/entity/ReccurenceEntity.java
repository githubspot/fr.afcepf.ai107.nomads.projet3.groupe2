package fr.afcepf.ai107.appnomads.entity;

import java.util.List;

public class ReccurenceEntity
{
	int idReccurence;
	int dureeReccurence; // Je ne suis pas d'aacord avec le int car les reccurence se calcule sur le temps, on devrait donc utiliser un type Date ou OffSetDateTime
	int ecartReccurence; // De même sur cette attribut
	
	List<PrestationEntity> prestations;
	LibelleReccurenceEntity libelleReccurence;
	LibelleDureeReccurenceEntity libelleDureeReccurence;
	/**
	 * @param idReccurence
	 * @param dureeReccurence
	 * @param ecartReccurence
	 * @param prestations
	 * @param libelleReccurence
	 * @param libelleDureeReccurence
	 */
	public ReccurenceEntity(int idReccurence, int dureeReccurence, int ecartReccurence,
			List<PrestationEntity> prestations, LibelleReccurenceEntity libelleReccurence,
			LibelleDureeReccurenceEntity libelleDureeReccurence)
	{
		super();
		this.idReccurence = idReccurence;
		this.dureeReccurence = dureeReccurence;
		this.ecartReccurence = ecartReccurence;
		this.prestations = prestations;
		this.libelleReccurence = libelleReccurence;
		this.libelleDureeReccurence = libelleDureeReccurence;
	}
	public int getIdReccurence() {
		return idReccurence;
	}
	public void setIdReccurence(int idReccurence) {
		this.idReccurence = idReccurence;
	}
	public int getDureeReccurence() {
		return dureeReccurence;
	}
	public void setDureeReccurence(int dureeReccurence) {
		this.dureeReccurence = dureeReccurence;
	}
	public int getEcartReccurence() {
		return ecartReccurence;
	}
	public void setEcartReccurence(int ecartReccurence) {
		this.ecartReccurence = ecartReccurence;
	}
	public List<PrestationEntity> getPrestations() {
		return prestations;
	}
	public void setPrestations(List<PrestationEntity> prestations) {
		this.prestations = prestations;
	}
	public LibelleReccurenceEntity getLibelleReccurence() {
		return libelleReccurence;
	}
	public void setLibelleReccurence(LibelleReccurenceEntity libelleReccurence) {
		this.libelleReccurence = libelleReccurence;
	}
	public LibelleDureeReccurenceEntity getLibelleDureeReccurence() {
		return libelleDureeReccurence;
	}
	public void setLibelleDureeReccurence(LibelleDureeReccurenceEntity libelleDureeReccurence) {
		this.libelleDureeReccurence = libelleDureeReccurence;
	}
	
	
	
}
