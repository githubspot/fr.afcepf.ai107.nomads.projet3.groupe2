package fr.afcepf.ai107.appnomads.entity;

import java.util.List;

public class LibelleReccurenceEntity
{
	int idLibelleReccurence;
	String libelle;
	
	List<ReccurenceEntity> reccurences;

	/**
	 * @param idLibelleReccurence
	 * @param libelle
	 * @param reccurences
	 */
	public LibelleReccurenceEntity(int idLibelleReccurence, String libelle, List<ReccurenceEntity> reccurences)
	{
		super();
		this.idLibelleReccurence = idLibelleReccurence;
		this.libelle = libelle;
		this.reccurences = reccurences;
	}

	public int getIdLibelleReccurence() {
		return idLibelleReccurence;
	}

	public void setIdLibelleReccurence(int idLibelleReccurence) {
		this.idLibelleReccurence = idLibelleReccurence;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<ReccurenceEntity> getReccurences() {
		return reccurences;
	}

	public void setReccurences(List<ReccurenceEntity> reccurences) {
		this.reccurences = reccurences;
	}
	
	
}
