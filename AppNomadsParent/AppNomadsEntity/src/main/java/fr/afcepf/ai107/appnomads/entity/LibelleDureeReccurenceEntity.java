package fr.afcepf.ai107.appnomads.entity;

import java.util.List;

public class LibelleDureeReccurenceEntity
{
	int idLibelleDureeReccurence;
	String libelle;
	
	List<ReccurenceEntity> reccurences;

	/**
	 * @param idLibelleDureeReccurence
	 * @param libelle
	 * @param reccurences
	 */
	public LibelleDureeReccurenceEntity(int idLibelleDureeReccurence, String libelle,
			List<ReccurenceEntity> reccurences)
	{
		super();
		this.idLibelleDureeReccurence = idLibelleDureeReccurence;
		this.libelle = libelle;
		this.reccurences = reccurences;
	}

	public int getIdLibelleDureeReccurence() {
		return idLibelleDureeReccurence;
	}

	public void setIdLibelleDureeReccurence(int idLibelleDureeReccurence) {
		this.idLibelleDureeReccurence = idLibelleDureeReccurence;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<ReccurenceEntity> getReccurences() {
		return reccurences;
	}

	public void setReccurences(List<ReccurenceEntity> reccurences) {
		this.reccurences = reccurences;
	}
	
	
}
